#include "base64.h"
#include <iostream>
#include <WS2tcpip.h>
#include <string>
// #include "opencv.hpp"


#pragma comment (lib, "ws2_32.lib")

using namespace std;
// using namespace cv;

void main()
{
	// Initialze winsock
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		cerr << "Can't Initialize winsock! Quitting" << endl;
		return;
	}

	// Create a socket
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		cerr << "Can't create a socket! Quitting" << endl;
		return;
	}

	// Bind the ip address and port to a socket
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000);
	hint.sin_addr.S_un.S_addr = INADDR_ANY; // Could also use inet_pton .... 

	bind(listening, (sockaddr*)&hint, sizeof(hint));

	// Tell Winsock the socket is for listening 
	listen(listening, SOMAXCONN);

	// Wait for a connection
	sockaddr_in client;
	int clientSize = sizeof(client);

	SOCKET clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

	char host[NI_MAXHOST];		// Client's remote name
	char service[NI_MAXSERV];	// Service (i.e. port) the client is connect on

	ZeroMemory(host, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);

	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		cout << host << " connected on port " << service << endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		cout << host << " connected on port " <<
			ntohs(client.sin_port) << endl;
	}

	// Close listening socket
	closesocket(listening);

	// While loop: accept and send message back to client
	char buf[999000];

	while (true)
	{
		ZeroMemory(buf, 999000);

		// Wait for client to send data
		int bytesReceived = recv(clientSocket, buf, 999000, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			cerr << "Error in recv(). Quitting" << endl;
			break;
		}

		if (bytesReceived == 0)
		{
			cout << "Client disconnected " << endl;
			break;
		}

		cout << string (buf, 0, bytesReceived) << endl;

		// Decode to Image
		std::string encodedImage = buf;
		std::string decodedImage = base64_decode(encodedImage);


		// Split into 4 parts

		//cv::Mat image = decodeImage

		// cv::Mat top_left
			// = img(cv::Range(0, img.rows / 2 - 1), cv::Range(0, img.cols / 2 - 1));
		// cv::Mat top_right
			// = img(cv::Range(0, img.rows / 2 - 1), cv::Range(img.cols / 2, img.cols - 1));
		// cv::Mat bottom_left
			// = img(cv::Range(img.rows / 2, img.rows - 1), cv::Range(0, img.cols / 2 - 1));
		// cv::Mat bottom_right
			// = img(cv::Range(img.rows / 2, img.rows - 1), cv::Range(img.cols / 2, img.cols - 1));

		// cv::imshow("top_left", top_left);
		// cv::imshow("top_right", top_right);
		// cv::imshow("bottom_left", bottom_left);
		//cv::imshow("bottom_right", bottom_right);
		//cv::waitKey(0);
		//


		// Send back to client
		send(clientSocket, buf, bytesReceived + 1, 0);
		cout << "Data was sent back to client.. "<< endl;
	}

	// Close the socket
	closesocket(clientSocket);

	// Cleanup winsock
	WSACleanup();

	system("pause");
}